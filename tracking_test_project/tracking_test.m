clear all
close all
clc

files = dir('./data/img/');
points = [];
figure,
%Get detected rectangles 
[rect, rectdata] = rectangles();
for f = 3:length(files)
    %display(files(f).name);
    I = imread(files(f).name);
    sizes = size(I);    height = sizes(1);    width = sizes(2);    channels = sizes(3);
    grayImg = rgb2gray(I);   
    if f == 3  %first 2 elements are '.' and '..', third element contains the first file name 
        
        pointTracker = initPointTracker(grayImg, rectdata);        
    elseif f > 3
        if rem(f,4) == 0
        
         ff = f-2
         dat = rectdata(ff);
         %if size(dat, 1) > 0
         %setPoints(pointTracker, getFeaturePoints(I, dat));
         %end
        end
         [points,point_validity] = step(pointTracker, grayImg);
         %plot(points)
    end
    rgbimg = cat(3, grayImg, grayImg, grayImg);
    points = horzcat(points, ones(size(points, 1), 1)); 
    J = drawPoints(rgbimg, points);  
    J = drawRectangles(J, rectdata(f - 2));
    imshow(J);    
    fid = f - 2
 
end


