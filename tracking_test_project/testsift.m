clear all;
close all;
clc;

I1 = imread('./data/img/frame_0000.jpg');
%I2 = imresize(imrotate(I1,-20),1.2);
I2 = imread('./data/img/frame_0010.jpg');

figure,
% subplot(2, 2, 1) 
% subimage(I1);
% xlabel('width') 
% ylabel('height') 
%subplot(2, 2, 2); subimage(I2);
%subplot(5, 6, 3); subimage(I2);

gI1 = rgb2gray(I1);
gI2 = rgb2gray(I2);

[Gmag Gdir] = imgradient(gI1);

points1 = detectSURFFeatures(gI1);
points2 = detectSURFFeatures(gI2);

[f1,vpts1] = extractFeatures(gI1,points1);
[f2,vpts2] = extractFeatures(gI2,points2);

indexPairs = matchFeatures(f1,f2) ;
matchedPoints1 = vpts1(indexPairs(:,1));
matchedPoints2 = vpts2(indexPairs(:,2));

figure; showMatchedFeatures(I1,I2,matchedPoints1,matchedPoints2);
legend('matched points 1','matched points 2');