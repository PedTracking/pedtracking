function pointTracker = initPointTracker(initialFrame, rectdata)
pointTracker = vision.PointTracker();
 rect = [];
%first frame rectangles info 1:x 2:y 3:w 4:h 
rectdata1 = rectdata(1);
figure;
initPoints = getFeaturePoints(initialFrame, rectdata1);
initialize(pointTracker, initPoints, initialFrame);
end



