% function newfeatures = getFeaturesInRectangles(rectangles, features) 
% newfeatures = [];
% for i = 1:size(rectangles,1)
% x = rectangles(1); y = rectangles(2); w = rectangles(3); h = rectangles(4);
% uL = [x,y];
% %lower right point rectangle
% lLx = x;
% lLy = y + h;
% lL = [lLx lLy];
% uR = [(x + w), y];
% lR = [(x + w), (y + h)];
% 
% %if features(i, 1) < lR(1) && features(i, 1) 
% 
% for j = 1:size(features, 1)
%     loc = features(i).Location;
%     %X = [uL(1), uR(1), lL(1), lR(1)];
%     %Y = [uL(2), uR(2), lL(2), lR(2)];  
% %     loc(1, 1)
% %     loc(1, 2)
% %     uL(1, 1)
% %     uR(1, 1)
% %     uL(1, 2)
% %     lL(1, 2)
%     
% 
% % isInBox = @(M,B) (M(:,1)>B(1)).*(M(:,1)<B(1)+B(3)).*(M(:,2)>B(2)).*(M(:,2)<B(2)+B(4));
% % if isInBox(loc,rectangles(i)) == 1
% %     newfeatures = [newfeatures; features(i)]; 
% % end
% 
%     if loc(1,1) >= uL(1, 1) && loc(1,1) <= uR(1, 1) 
%         if loc(1,2) >= uL(1,2) && loc(1,2) <= lL(1,2)
%     newfeatures = [newfeatures; features(i)];   
%         end
%     end
%     
% %     if inpolygon(loc(1,1), loc(1,2), X, Y) == 1
% %         newfeatures = [newfeatures; features(i)];
% %     end
% end
% end

function newfeatures = getFeaturesInRectangles(rectangles, features)
newfeatures = [];
for i = 1:size(features, 1)
    floc = features(i).Location;
    for j = 1:size(rectangles, 1)
        x = rectangles(1); y = rectangles(2); w = rectangles(3); h = rectangles(4);
        uL = [x, y];
        lL = [x, (y + h)];
        uR = [(x + w), y];
    flocX = floc(1, 1);
    flocY = floc(1, 2);
    if flocX >= uL(1,1) && flocX <= uR(1,1)
        if flocY >= uL(1, 2) && flocY <= lL(1, 2);
            %features(i)
            newfeatures = [newfeatures; features(i).Location];
        end                   
    end
    end
end
end
