%draw points 
function resI = drawPoints(I, points)
if size(I, 3) == 1
    I = cat(3, I, I, I);
end
resI = insertShape(I, 'Circle', points);
end

