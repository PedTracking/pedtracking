clear all
close all
clc
% 
% A = [1 1 1; 1 2 3; 2 3 1]
% B = [0; 0; 0]
% C = horzcat(A, B)
% D = 0;


I = imread('frame_0000.jpg');
grayI = rgb2gray(I);
pointsToDraw = detectSurfFeatures(grayI);



%%getResetPoints test
[rect1 rect2] = rectangles();
points = getFeaturePoints(grayI, rect2(1));


%imshow(I(10:30, 10:50));
%row1 = ypoint; 
%row2 = ypoint + height;
%col1 = xpoint;
%col2 = xpoint + width;

rgbI = cat(3, grayI, grayI, grayI);

pt = zeros(size(points, 1), 3);
for i = 1:size(points, 1)
    pt(i, 1) = points(i, 1);
    pt(i, 2) = points(i, 2);
    pt(i, 3) = 2;
end

J = drawPoints(rgbI, pt);
figure, imshow(J);
%hold on
%plot(points);
%hold off