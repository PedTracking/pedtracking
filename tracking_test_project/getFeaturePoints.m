function  initPoints  = getFeaturePoints( frame, rectdata1 )
%Get feature points of detected rectangles4
%Input rectdata1: Mx4 array col1: x col2: y col3: w col4: h
%Returns a Mx2 array which contains detected feature points location 
%These feature points are passed to KLT for tracking
initPoints = [];
%figure,
%Iterate to each rectangle detected in the frame
for i = 1:size(rectdata1, 1)     
    rect1 = rectdata1(i,:);
    x = rect1(1); y = rect1(2); w = rect1(3); h = rect1(4);
    %Take rectangle into account only if width > 10 pixels
    if w > 10        
    %calculate rows
    row1 = y; 
    row2 = row1 + h;
    %calculate cols
    col1 = x;
    col2 = col1 + w;
    rectUL = [col1 row1];
    %FIX: when rectangle value coordinates are negative take their absolute
    %value
    col1 = abs(col1);    col2 = abs(col2);    row1 = abs(row1);    row2 = abs(row2);
    %I -> detected rectangle
    I = frame(uint8(row1):uint8(row2),uint8(col1):uint8(col2));  
    points = detectSurfFeatures(I);
    points = vertcat(detectHarrisCornerFeatures(I));
    %resI = drawPoints(I, points);
    %imshow(resI);
    %waitforbuttonpress;
    %Translate detected points to original image coordinates
    newpoints = [];
    for j = 1:size(points, 1)
        %New x value
        newx = points(j, 1) + col1;
        %New y value
        newy = points(j, 2) + row1;
        %New point array
        newpoint = [newx newy];
        %Append point to translated points matrix
        newpoints = [newpoints ; newpoint];
    end
    initPoints = [initPoints; newpoints];
    end  
end
if size(initPoints, 1) == 0
    initPoints = ones(1,2);
end
end

