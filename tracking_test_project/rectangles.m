function [maprect, maprect2] = rectangles()
%
%
%Returns a 1x2 array containing two maps 
%maprect = {frameid:[xrectcenter, yrectcenter]}
%maprect2 = {frameid:[x y w h]}
f = fopen('./input.txt', 'r');
maprect = containers.Map('KeyType','int32','ValueType','any');
maprect2 = containers.Map('KeyType', 'int32', 'ValueType', 'any');
fnr = 0;
while true
    l = fgetl(f); 
    if ~ischar(l);
        break;
    end
    rectangle = strsplit(l, ' ');
    if length(rectangle) < 2        
        fnr = fnr + 1;        
    else        
    x = str2double(rectangle(1));
    y = str2double(rectangle(2));
    w = str2double(rectangle(3));
    h = str2double(rectangle(4));
    xcenter = x + w/2;
    ycenter = y + h/2;  
    pcenter = [xcenter ycenter];  
    rectdata = [x y w h];
    if isKey(maprect, fnr)
        maprect(fnr) = [maprect(fnr); pcenter];
        maprect2(fnr) = [maprect2(fnr); rectdata];
        %rectg = [rectg pcenter];
    else
        maprect(fnr) = pcenter;
        maprect2(fnr) = rectdata;
    end
    end    
end

fclose(f);
end
