clear all
close all
clc

files = dir('./data/img/');
points = [];
figure,
%Get detected rectangles 
[rect, rectdata] = rectangles();

lastfeatures = [];
currentfeatures = [];
for f = 3:length(files)
    I = imread(files(f).name);
    grayImg = rgb2gray(I);
    frameID = f - 2
    %currentfeatures = detectSURFFeatures(grayImg);
    currentfeatures = detectSurfFeatures(grayImg);
    J = drawPoints(grayImg, currentfeatures);
    J = drawRectangles(J, rectdata(f - 2));
    imshow(J);
    waitforbuttonpress;
    hold on;
    plot(currentfeatures);
    hold off;
    waitforbuttonpress;
    rectdata(f - 2)
    currentfeatures = getFeaturesInRectangles(rectdata(f - 2), currentfeatures);
    if f > 3
    [f1,vpts1] = extractFeatures(grayImg, currentfeatures);
    [f1, vpts2] = extractFeatures(grayImg, lastfeatures);
    indexPairs = matchFeatures(f1,f2);
    matchedPoints1 = vpts1(indexPairs(:,1));
matchedPoints2 = vpts2(indexPairs(:,2));
    showMatchedFeatures(I1,I2,matchedPoints1,matchedPoints2);
    waitforbuttonpress;
    end
    lastfeatures = currentfeatures;
end