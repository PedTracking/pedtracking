function pointsToDraw = detectSurfFeatures( I )
%Returns a Mx3 array (col1: x , col2: y , col3: radius (for point drawing))
%
points = detectSURFFeatures(I);
points2 = zeros(size(points, 1), 3);
pointsToDraw = [];
for i = 1:size(points, 1)
    loc = points(i).Location;
    orient = points(i).Orientation;
    points2(i, 1) = loc(1);
    points2(i, 2) = loc(2);
    points2(i, 3) = orient;    
end
p1 = points2(:,1:2);
p2 = ones(size(p1, 1), 1);

pointsToDraw = horzcat(p1, p2);
end





