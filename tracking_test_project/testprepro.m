close all;
clear all;
clc
I = imread('frame_0000.jpg');
[featureVector, hogVisualization] = extractHOGFeatures(I);
figure;
imshow(I);
plot(hogVisualization);


I = rgb2gray(I);
thresh = multithresh(I, 10);
segI = imquantize(I, thresh);
RGB = label2rgb(segI);

cannyI = edge(rgb2gray(RGB), 'Canny');

%imtool(cannyI);
%figure, imshow(cannyI);