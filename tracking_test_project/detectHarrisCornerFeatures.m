function points = detectHarrisCornerFeatures( I )
%Get Harris Features
%  
points = zeros(1,3);
if size(I, 1) > 5 && size(I, 2) > 5
features = detectHarrisFeatures(I);
points = features.selectStrongest(100);
p = [];
for i = 1:size(points, 1)
    p = [p; points.Location];
end
points = horzcat(p, ones(size(p, 1), 1));
end
end

