function maprect = rectangles()
f = fopen('input.txt', 'r');
frames = cell(435, 1);
rectangles = zeros(1, 2);
maprect = containers.Map('KeyType','int32','ValueType','any');
fnr = 0;
while true
    l = fgetl(f); 
    if ~ischar(l);
        break;
    end
    rectangle = strsplit(l, ' ');
    if length(rectangle) < 2        
        fnr = fnr + 1;        
    else        
    x = str2double(rectangle(1));
    y = str2double(rectangle(2));
    w = str2double(rectangle(3));
    h = str2double(rectangle(4));
    xcenter = x + w/2;
    ycenter = y + h/2;  
    pcenter = [xcenter ycenter];    
    if isKey(maprect, fnr)
        maprect(fnr) = [maprect(fnr);pcenter];
        %rectg = [rectg pcenter];
    else
        maprect(fnr) = pcenter;
    end
    end    
end
fclose(f);
end
