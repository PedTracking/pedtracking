function [itls, etime] = getitl(rectangles)
%GETITL Summary of this function goes here
%   Detailed explanation goes here

% parameters
param.similarity_method =  'ihtls';
param.min_s = 0.0100;
param.debug = 0;
param.hor = 20;
param.eta_max = 2;

%Number of frames
framesNr = length(rectangles) - 1
for i = 1:framesNr
    idl = generateIdl(rectangles(i), rectangles(i+1));
    [itl etime] = smot_associate(idl,param)
    if isfield(itl, {'t_start', 't_end', 'length', 'data', 'omega'})
    %(i).t_start = itl.t_start;
    %itls(i).t_end = itl.t_end;
    %itls(i).length = itl.length;
    %itls(i).omega = itl.omega;
    %itls(i).data = itl.data;
    end
end

end

function idl = generateIdl(xy1, xy2)
%Min value between rect detected
minV = min(length(xy1), length(xy2));
for i = 1:minV
    idl(i).xy = [xy1(i,:); xy2(i,:)];
end
end
